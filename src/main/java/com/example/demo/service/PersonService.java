package com.example.demo.service;

import com.example.demo.model.Person;
import com.example.demo.repository.PersonRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PersonService {

    private PersonRepository personRepository;

    public List<Person> getAll(){
        return (List<Person>) personRepository.findAll ();
    }

    public Person getById(Long id){
        return (Person) personRepository.findAllById ( id );
    }

    public Person  create (Person person){
        return personRepository.save ( person );
    }

    public Person  update (Person person){
        return personRepository.save ( person );
    }

    public void delete(Long id){
        personRepository.deleteById ( id );
    }


    public Person save(Person person) {
    }

    public Person edit(Person person) {
    }
}

