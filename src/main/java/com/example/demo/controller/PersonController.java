package com.example.demo.controller;

import com.example.demo.model.Person;
import com.example.demo.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PersonController {
    private final PersonService personService;

    public PersonController(PersonService personService){
        this.personService = personService;
    }

    @GetMapping("/api/v2/person")
    public ResponseEntity<?> getPerson(){
        return ResponseEntity.ok(personService.getAll());
    }

    @PostMapping("/api/v2/person")
    public ResponseEntity<?> addPerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.save(person));
    }

    @PutMapping("/api/v2/person")
    public ResponseEntity<?> editPerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.edit(person));
    }

    @DeleteMapping("person/{id}")
    public void deletePerson(@PathVariable Long id) {
        personService.delete(id);
    }


}
